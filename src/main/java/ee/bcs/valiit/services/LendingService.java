package ee.bcs.valiit.services;

import java.sql.ResultSet;
import java.sql.SQLException;

import ee.bcs.valiit.model.Book;
import ee.bcs.valiit.model.Order;
import ee.bcs.valiit.model.User;

public class LendingService {

	private static final String statusIn = "Kohal";
	private static final String statusOut = "Laenutuses";

	public static boolean isBookFree(int bookId) {
		String sql = "select * from book where id='" + bookId + "' AND status='" + statusIn + "'";
		ResultSet result = BookService.executeSql(sql);
		try {
			if (result != null && result.next()) {
				return true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	public static void addBookToOrder(User user, int bookId, Order order) {
		if (isBookFree(bookId)) {

			String insertSql = String.format("INSERT INTO order_item (book_id, order_id) VALUES (%s, %s)", bookId,
					order.getId());
			BookService.executeSql(insertSql);
		}
	}

	public static Order getOrCreateOpenOrder(int userId) {
		String sql = "SELECT * FROM `order` WHERE user_id = " + userId + " and is_open = true";
		ResultSet result = BookService.executeSql(sql);
		try {
			if (result != null && result.next()) {
				Order order = new Order();
				order.setId(result.getInt("id"));
					return order;
			} else {
				// Avatud ostukorvi ei leitud. Tekitame selle...
				String sql2 = String.format("INSERT INTO `order` (is_open, user_id) VALUES (true, %s)", userId);
				BookService.executeSql(sql2);
				return getOrCreateOpenOrder(userId);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}			
		return null;
	}
	
	
	
	

	public static String confirmOrder(int orderId) {
		
		String sql = String.format("UPDATE book SET book.status = 'Laenutuses' WHERE id in (SELECT order_item.book_id from order_item where order_item.order_id = %s)", orderId);
		BookService.executeSql(sql);
		
		String sql3 = String.format("update `order` set is_open = false where id = %s", orderId);
		BookService.executeSql(sql3);
		
		return "SUCCESS";
	}

}

