package ee.bcs.valiit.services;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import ee.bcs.valiit.model.Book;
import ee.bcs.valiit.model.LentBook;
import ee.bcs.valiit.model.Order;
import ee.bcs.valiit.model.User;

public class BookService {

	public static final String SQL_CONNECTION_URL = "jdbc:mysql://localhost:3306/my_library";
	public static final String SQL_USERNAME = "root";
	public static final String SQL_PASSWORD = "tere";

	public static ResultSet executeSql(String sql) {
		try {
			Class.forName("org.mariadb.jdbc.Driver");
			try (Connection conn = DriverManager.getConnection(SQL_CONNECTION_URL, SQL_USERNAME, SQL_PASSWORD)) {
				try (Statement stmt = conn.createStatement()) {
					return stmt.executeQuery(sql);
				}
			}
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static List<Book> getBooks() {
		List<Book> books = new ArrayList<Book>();
		try {
			ResultSet result = executeSql("select * from book");
			if (result != null) {
				while (result.next()) {
					Book book = new Book();
					book.setId(result.getInt("id"));
					book.setAuthor(result.getString("authors"));
					book.setTitle(result.getString("title"));
					book.setDescription(result.getString("description"));
					book.setStatus(result.getString("status"));
					books.add(book);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();

		}
		return books;
	}

	public static void addBook(Book book) {
		if (bookNotExists(book.getAuthor())) {
			String sql = "INSERT INTO book (authors, title, description, status) " + "VALUES ('" + book.getAuthor()
					+ "', '" + book.getTitle() + "', '" + book.getDescription() + "', '" + book.getStatus() + "')";
			executeSql(sql);
		}
	}

	public static boolean bookNotExists(String author) {
		return getBookByAuthor(author) == null;
	}

	public static Book getBookByAuthor(String author) {
		try {
			ResultSet result = executeSql("select * from book where authors LIKE '%" + author + "%'");
			if (result != null) {
				if (result.next()) {
					Book book = new Book();
					book.setId(result.getInt("id"));
					book.setAuthor(result.getString("authors"));
					book.setTitle(result.getString("title"));
					book.setDescription(result.getString("description"));
					book.setStatus(result.getString("status"));
					return book;
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return null;
	}

	public static Book getBook(int bookId) {
		try {
			String sql = "select * from book where id=" + bookId;
			ResultSet result = executeSql(sql);
			if (result != null) {
				if (result.next()) {
					Book book = new Book();
					book.setId(result.getInt("id"));
					book.setAuthor(result.getString("authors"));
					book.setTitle(result.getString("title"));
					book.setDescription(result.getString("description"));
					book.setStatus(result.getString("status"));
					return book;
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static void modifyBook(Book book) {
		String sql = String.format(
				"UPDATE book SET authors = '%s', title = '%s', description = '%s', status = '%s' WHERE id = %s",
				book.getAuthor().replaceAll("'", "''"), book.getTitle().replaceAll("'", "''"), book.getDescription().replaceAll("'", "''"), book.getStatus(), book.getId());
		executeSql(sql);
	}

	public static void deleteBook(int bookId) {
		String sql = "DELETE FROM book where id = " + bookId;
		executeSql(sql);
	}
	
	

	public static String lendBook(int bookId, User user) {
		// tuvasta, kas ostukorv on olemas
		// kui olemas, siis päri selle ostukorvi info 

		
		Order order = LendingService.getOrCreateOpenOrder(user.getId());

		if(LendingService.isBookFree(bookId)) {
			LendingService.addBookToOrder(user, bookId, order);
			String sql11 = "UPDATE book SET book.status = 'Vormistamisel' where id = " + bookId;
			executeSql(sql11);
			return "SUCCESS";
		}
		return "FAIL";
	}
	
	
//	public static String checkout(int bookId, User user) {
//		Order order = LendingService.getOrCreateOpenOrder(order.getId());
//		LendingService.confirmOrder(bookId, user);
//		return "SUCCESS";
//		
//	}
//	

	public static List<Book> searchBook(String searchText) {
		List<Book> books = new ArrayList<>();
		try {
			ResultSet result = executeSql(
					"select * from book where authors LIKE '%" + searchText + "%' OR title LIKE '%" + searchText
							+ "%' OR description LIKE '%" + searchText + "%' OR status LIKE '%" + searchText + "%'");
			if (result != null) {
				while (result.next()) {
					Book book = new Book();
					book.setAuthor(result.getString("authors"));
					book.setTitle(result.getString("title"));
					book.setDescription(result.getString("description"));
					book.setStatus(result.getString("status"));
					books.add(book);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return books;
	}

	public static List<Book> getBooksInOrder(int orderId) {
		List<Book> books = new ArrayList<>();
		try {
			String sql = "select book.* from book inner join order_item on order_item.book_id = book.id and order_item.order_id = " + orderId;
			ResultSet result = executeSql(sql);
			if (result != null) {
				while (result.next()) {
					Book book = new Book();
					book.setId(result.getInt("id"));
					book.setTitle(result.getString("title"));
					book.setAuthor(result.getString("authors"));
					books.add(book);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return books;
	}

	public static void deleteMyCartItem(int bookId) {
		String sql = "DELETE FROM order_item where book_id = " + bookId;
		executeSql(sql);
		String sql12 = "UPDATE book SET book.status = 'Kohal' where id = " + bookId;
		executeSql(sql12);
	}

	public static List<LentBook> getBooksInLending() {
		List<LentBook> books = new ArrayList<>();
		try {
	
			String sql = 
					"select u.id as user_id, u.first_name, u.last_name, u.email, books_in_lending.id as book_id, books_in_lending.authors, books_in_lending.title " +
			        "from user u inner join (" +
					"select book.*, (select `order`.user_id from `order` inner join order_item on `order`.id = order_item.order_id " +
					"where order_item.book_id = book.id order by order_item.id desc limit 1) as user_id " +
					"from book where book.status = 'Laenutuses') as books_in_lending on books_in_lending.user_id = u.id";

			ResultSet result = executeSql(sql);
			if (result != null) {
				while (result.next()) {
					LentBook lentBook = new LentBook();
					lentBook.setFirstName(result.getString("first_name"));
					lentBook.setLastName(result.getString("last_name"));
					lentBook.setEmail(result.getString("email"));
					lentBook.setTitle(result.getString("title"));
					lentBook.setAuthors(result.getString("authors"));
					books.add(lentBook);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return books;
	}
}
