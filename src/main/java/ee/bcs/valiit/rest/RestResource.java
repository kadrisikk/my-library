package ee.bcs.valiit.rest;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.Consumes;
import javax.ws.rs.CookieParam;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.NewCookie;
import javax.ws.rs.core.Response;

import ee.bcs.valiit.model.Book;
import ee.bcs.valiit.model.LentBook;
import ee.bcs.valiit.model.Order;
import ee.bcs.valiit.model.User;
import ee.bcs.valiit.services.AuthenticationService;
import ee.bcs.valiit.services.BookService;
import ee.bcs.valiit.services.LendingService;

@Path("/")
public class RestResource {

	@GET
	@Path("/get_books_for_admin")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Book> getBooksForAdmin(@Context HttpServletRequest req) {
		if (isUserAuthorized(req, "admin")) {
			return BookService.getBooks();
		} else {
			return new ArrayList<>();
		}
	}

	@GET
	@Path("/get_books")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Book> getBooks(@Context HttpServletRequest req) {
		// if (isUserAuthorized(req, "admin") || isUserAuthorized(req, "user")) {
		return BookService.getBooks();
		// } else {
		// return new ArrayList<>();
		// }
	}

	@GET
	@Path("/get_books_byauthor")
	@Produces(MediaType.APPLICATION_JSON)
	public Book getBookByAuthor(@QueryParam("book_author") String bookAuthor) {
		return BookService.getBookByAuthor(bookAuthor);
		
	}
	
	@GET
	@Path("/get_lending_cart")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Book> getLendingCart(@Context HttpServletRequest req) {
		User user = getAuthenticatedUser(req);
		if (user.getId() > 0) {
			Order openOrder = LendingService.getOrCreateOpenOrder(user.getId());
			return BookService.getBooksInOrder(openOrder.getId());
		} else {
			return new ArrayList<>();
		}
	}
	
	@GET
	@Path("/get_books_in_lending")
	@Produces(MediaType.APPLICATION_JSON)
	public List<LentBook> getBooksInLending(@Context HttpServletRequest req) {
		User user = getAuthenticatedUser(req);
		if (user.getId() > 0) {
			return BookService.getBooksInLending();
		} else {
			return new ArrayList<>();
		}
	}

	@GET
	@Path("/get_book")
	@Produces(MediaType.APPLICATION_JSON)
	public Book getBook(@QueryParam("book_id") int bookId) {
		return BookService.getBook(bookId);
	}

	@GET
	@Path("/search_book")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Book> searchBook(@QueryParam("search_text") String searchText) {
		return BookService.searchBook(searchText);
	}

	@GET
	@Path("/lend_book")
	@Produces(MediaType.APPLICATION_JSON)
	public String lendBook(@Context HttpServletRequest req, @QueryParam("book_id") int bookId) {
		User user = getAuthenticatedUser(req);
		if (user.getId() > 0) {
			return BookService.lendBook(bookId, user);
		} else {
			return "FAIL";
		}
	}
	
	@POST
	@Path("/confirm_order")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	public String confirmOrder(@Context HttpServletRequest req, @QueryParam("order_id") int orderId) {
		User user = getAuthenticatedUser(req);
		if (user.getId() > 0) {
			Order closeOrder = LendingService.getOrCreateOpenOrder(user.getId());
			return LendingService.confirmOrder(closeOrder.getId());
//		 return LendingService.confirmOrder(orderId);
		}
		return "FAIL";
	}


	@POST
	@Path("/add_book")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	public String addBook(Book book) {
		BookService.addBook(book);
			return "ok";
		}
	

	@POST
	@Path("/modify_book")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	public String modifyBook(Book book) {
		BookService.modifyBook(book);
		return "OK";
	}

	@POST
	@Path("/delete_book")
	@Produces(MediaType.TEXT_PLAIN)
	public String deleteBook(@FormParam("book_id") int bookId) {
		BookService.deleteBook(bookId);
		return "OK";
	}
	
	
	
	@POST
	@Path("/delete_my_cart_item")
	@Produces(MediaType.TEXT_PLAIN)
	public String deleteMyCartItem(@FormParam("book_id") int bookId) {
		BookService.deleteMyCartItem(bookId);
		return "OK";
	}

	@GET
	@Path("/set_cookie")
	@Produces(MediaType.TEXT_PLAIN)
	public Response setCookie() {
		NewCookie myCookie = new NewCookie("MY_TEST_COOKIE", String.valueOf(Math.random()), "/", null, null, -1, false,
				false);

		return Response.ok("Cookie set successfully!").cookie(myCookie).build();
	}

	@GET
	@Path("/read_cookie")
	@Produces(MediaType.TEXT_PLAIN)
	public String readCookie(@CookieParam("MY_TEST_COOKIE") String myCookie) {
		return "The cookie value was " + myCookie;
	}

	@GET
	@Path("/set_session_info")
	@Produces(MediaType.TEXT_PLAIN)
	public String setSessionInfo(@Context HttpServletRequest req) {
		HttpSession session = req.getSession(true);
		String myTestAttr = (String) session.getAttribute("TEST");
		if (myTestAttr != null) {
			return "Session attribute found: " + myTestAttr;
		} else {
			// Session attribute was not found.
			session.setAttribute("TEST", String.valueOf(Math.random()));
			return "Session attribute generated: " + (String) session.getAttribute("TEST");
		}
	}

	@POST
	@Path("/authenticate_user")
	@Produces(MediaType.TEXT_PLAIN)
	public String authenticateUser(@Context HttpServletRequest req, @FormParam("email") String email,
			@FormParam("password") String password) {
		User user = AuthenticationService.getUser(email, password);
		if (user == null) {
			// Autentimine ebaõnnestus
			return "FAIL";
		} else {
			// Autentimine õnnestus
			HttpSession session = req.getSession((true));
			session.setAttribute("AUTH_USER", user);
			return "SUCCESS";
		}

	}

	@GET
	@Path("/get_authenticated_user")
	@Produces(MediaType.APPLICATION_JSON)
	public User getAuthenticatedUser(@Context HttpServletRequest req) {
		HttpSession session = req.getSession((true));
		if (session.getAttribute("AUTH_USER") != null) {
			// Kasutaja on sisse loginud
			return (User) session.getAttribute("AUTH_USER");
		} else {
			// Kasutaja ei ole sisse loginud
			return new User();
		}
	}

	@GET
	@Path("/logout")
	@Produces(MediaType.TEXT_PLAIN)
	public String logout(@Context HttpServletRequest req) {
		HttpSession session = req.getSession((true));
		session.removeAttribute("AUTH_USER");
		return "SUCCESS";
	}

	private boolean isUserAuthorized(@Context HttpServletRequest req, String expectedRole) {
		HttpSession session = req.getSession((true));
		User user = null;
		if (session.getAttribute("AUTH_USER") != null) {
			// Kasutaja on sisse loginud
			user = (User) session.getAttribute("AUTH_USER");
			return user.getRole().equals(expectedRole);
		}
		return false;
	}
}
