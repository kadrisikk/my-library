function logInUser() {
	var email = $('#email').val();
	var password = $('#password').val();
	
	$.ajax(
		{
			url: '/my_library/rest/authenticate_user',
			method: 'POST',
			data: {
				'email': email,
				'password': password
				
			},
			complete: function (result) {
				// Logimisprotseduur lõppenud.
				if (result.responseText == 'SUCCESS') {
					// Logimine õnnestus.
					document.location = 'userlogin.html';
					
				} else {
					// Logimine ebaõnnestus.
				
					$('#errorBox').show();
				}
			}
		}
	);
}

function logOutUser() {
	$.ajax(
		{
			url: '/my_library/rest/logout',
			method: 'GET',
			complete: function (result) {
				document.location = 'index.html';
				$('#loginBox').show();
				$('#loginButton').show();
				$('#logoutButton').hide();
				$('#logoutBox').hide();
				$('#errorBox').hide();
			}
		}
	);
}

function getAuthenticatedUser(onRequestCompleteHandler) {
	$.ajax(
		{
			url: '/my_library/rest/get_authenticated_user',
			method: 'GET',
			complete: function(result) {
				onRequestCompleteHandler(result);
			}
		}
	);
}

function authUserRequestCompleted(result) {
	var user = result.responseJSON;
	if (user != null && user.id > 0) {
		// Kasutaja on sisse loginud. Kuvame logout nupu.
		$('#logoutButton').show();
		$('#loginButton').hide();
		$('#lending_btn').show();
		$('#lend_btn').show();
		$('#lend_btn2').show();
	} else {
		// Kasutaja ei ole sisse loginud. Kuvame login-kasti.
		$('#loginButton').show();
		$('#logoutButton').hide();
		$('#lending_btn').hide();
		$('#lend_btn').hide();
		$('#lend_btn2').hide();
	}
}
