
function configureTopMenu(result) {
	var user = result.responseJSON;
	if (user.role == 'admin') {
		$('#adminBooksLink').show();
		$('#adminOverviewLink').show();
		$('#userBooksLink').show();
		$('#unauthUserBooksLink').hide();
	} else if (user.role == 'user') {
		$('#userBooksLink').show();
		$('#unauthUserBooksLink').hide();
	}
}